let exec = require('child_process').exec,
    config = require('./config');


module.exports = {

    findIfDBRunningOnUserDefinedPort: function () {

        return new Promise((resolve, reject) => {

            exec(`lsof -iTCP -sTCP:LISTEN | grep mongo`, function (error, stdout, stderr) {

                //if DB is not running, an error would be thrown and then MongoDB would be started on User Defined Port
                if (error) {
                    console.log('exec error: ' + error);
                    resolve(false);
                } else if (stderr) {
                    console.log('stderr error: ' + stderr);
                    resolve(false);
                }
                //If DB is running, then the below block will find the port on which it's running
                //If the DB is already running on the User defined port, a message would be shown to the user
                //Else the DB would be started on the User defined Port
                else {
                    if (stdout.includes(`localhost:${config.getGlobalVariable('databasePort')}`)) {
                        console.log(`mongoDB is already running on the User selected Port: ${config.getGlobalVariable('databasePort')}, please change the port`);
                        resolve(true);
                    } else {
                        console.log(`mongoDB is NOT already running on the User selected Port: ${config.getGlobalVariable('databasePort')}`);
                        resolve(false);
                    }
                }


            });
        }).catch(function (error) {

            console.log('Error Found while finding the port of Mongo DB', error);
        });
    },

    startMongoDb: async function () {

        let isDbRunning = await this.findIfDBRunningOnUserDefinedPort();
        console.log('Mongo DB running state: ', isDbRunning);

        if (!isDbRunning) {

            await this.runMongoDbCommand();

        }
    },

    runMongoDbCommand: function () {

        return new Promise((resolve, reject) => {

            exec(`mongod --dbpath ${config.getGlobalVariable('softwareFolderPath')}/data/db/ --port ${config.getGlobalVariable('databasePort')}`, function (error, stdout, stderr) {

                if (error) {

                    if (error.includes('Another mongod instance is already running')) {
                        console.log(`Another Mongo DB instance is already running on the ${config.getGlobalVariable('softwareFolderPath')}/data/db/ directory, Please change the Directory`)
                        reject(error);

                    } else {
                        reject(error);
                    }

                }
                else if (stderr) {
                    reject(error);

                }
                else {
                    console.log(`Mongo DB Started on Port: ${config.getGlobalVariable('databasePort')}`);
                    resolve(stdout);

                }
            });
        }).catch(function (error) {

            console.log(error);

        })

    }
};