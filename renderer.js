// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const exec = require('child_process').exec;
const fs = require('fs');
const config = require('./config');
const installMongoDb = require('./installMongoDb');
const runMongoDb = require('./runMongoDb');
const api = require('./startApi');
let softwareFolderPath;


//get the File Path Button and add an event listener for the click event
const filePathButton = document.getElementById('filePath');
filePathButton.addEventListener('click', () => {
    "use strict";

    //Click on the Browse button to select the appropriate Directory for installation of the Software
    const {dialog} = require('electron').remote;

    softwareFolderPath = dialog.showOpenDialog({
        properties: ['openDirectory']
    });

    //Display the selected Directory Path
    document.getElementById('path').innerHTML = 'Directory Path: ' + softwareFolderPath;
    config.setGlobalVariable('softwareFolderPath', softwareFolderPath);

});


//Get the form data and start the installation
const installButton = document.getElementById('install');
installButton.addEventListener('click', async function setup() {

    let getAllInputFields = document.querySelectorAll("input[type=text]");
    for (let i=0; i<getAllInputFields.length; i++){
        let inputField = getAllInputFields[i];
        let inputFieldText = inputField.value;
        config.setGlobalVariable(inputField.id, parseInt(inputFieldText));

    }

    if(softwareFolderPath) {
        if ( (config.getGlobalVariable('databasePort') >= 27018) && (config.getGlobalVariable('databasePort') <= 37018)) {
            installMongoDb.installDatabase(softwareFolderPath);
            runMongoDb.startMongoDb();
            api.startApi();
            console.log('renderer js')

        } else{
            alert('Provide the port between 27018 to 37018');
        }
    } else{
        alert('Please select a Directory');
    }

});