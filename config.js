let globalVariables = {};

let config = {


    setGlobalVariable: function (name, value) {

        globalVariables[name] = value;

    },


    getGlobalVariable: function (name) {

        return globalVariables[name];

    }
};


module.exports = config;