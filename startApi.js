let exec = require('child_process').exec,
    config = require('./config'),
    fs = require('fs');
    apiServer = '/Users/sjasu1/Desktop/SeleniumGrid/db-operations-api/server.js';


const runCommandToStartAPI = new Promise(
    (resolve, reject) =>{

    exec(`node ${apiServer}`, function (error, stdout, stderr) {
        if (error) {
            console.log('exec error: ' + error);
            reject(error);
        } else if (stderr) {
            console.log('stderr error: ' + stderr);
            reject(stdout);
        }
        else {

            console.log('API Started');
            resolve(stdout);
        }
    });



});

module.exports = {
    startApi: function () {

        runCommandToStartAPI.then(fulfilled => console.log('Promise Value', fulfilled))
            .catch(error => console.log(error.message));
    }
};


// module.exports = {
//
//     runApiCommand: function () {
//
//         return new Promise((resolve, reject) => {
//
//             exec(`node ${apiServer}`, function (error, stdout, stderr) {
//             //if DB is not running, an error would be thrown and then MongoDB would be started on User Defined Port
//             if (error) {
//                 console.log('exec error: ' + error);
//                 reject(error);
//             } else if (stderr) {
//                 console.log('stderr error: ' + stderr);
//                 reject(stdout);
//             }
//             else {
//
//                 console.log('API Started');
//                 resolve(stdout);
//             }
//         });
//     }).catch(function (error) {
//
//             console.log('Error Found while finding the port of Mongo DB', error);
//         });
//     },
//
//     start: async function () {
//
//         await this.runApiCommand();
//
//     }
// };