const exec = require('child_process').exec;
const fs = require('fs');

module.exports = {

    isMongoDbInstalled: function () {

        return new Promise((resolve, reject) => {
            exec(`command -v mongod`, function (error, stdout, stderr) {
                if (error) {
                    console.log('exec error: ' + error);
                    reject(error);
                } else if (stderr) {
                    console.log('stderr: ' + stderr);
                    reject(stderr);
                }
                else {

                    if (stdout.includes('mongod')) {
                        console.log('Mongo DB is already installed on the machine');
                        resolve(true);
                    } else {
                        console.log('Mongo DB not alreadt installed on the machine');
                        resolve(false);
                    }

                }
            });
        }).catch(function (error) {

            console.log('Error Found', error);
        })
    },

    runCommandToInstallMongoDb: function () {

    return new Promise((resolve, reject) => {
        exec(`brew install mongodb`, function (error, stdout, stderr) {
            if (error) {
                console.log('exec error: ' + error);
                reject(error);
            } else if (stderr) {
                console.log('stderr: ' + stderr);
                reject(stderr);
            }
            else {
                resolve(stdout);
            }
        });
    }).catch(function (error) {

        console.log('Error Found: ', error);

    });
},

    installDatabase: async function (softwareFolderPath) {
    let mongoDbState = await this.isMongoDbInstalled();

        this.createDataFolder(softwareFolderPath);
        if (!mongoDbState) {
            console.log('Installing Mongo DB');
            this.runCommandToInstallMongoDb();
            console.log('Mongo DB installed successfully')

        }
},

    createDataFolder: function (softwareFolderPath) {

        const MONGO_DB_FOLDER_PATH = `${softwareFolderPath}/data`;

        if (!fs.existsSync(MONGO_DB_FOLDER_PATH)) {
            fs.mkdirSync(MONGO_DB_FOLDER_PATH);
            fs.mkdirSync(`${MONGO_DB_FOLDER_PATH}/db`);
            console.log('Data folder created');
        } else {
            console.log('Data Folder already exists');
        }
    }
};
